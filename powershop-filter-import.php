<?php

/**
 * Plugin Name: Powershop Filter Import
 * Plugin URI: https://peanutbutter.es
 * Description: Filters the products that are imported from powershop 
 * Version: 1.0.3
 * Author: Pablo Giralt
 * Author URI: https://peanutbutter.es
 * License: GPL-3.0+
 * License URI: http://www.gnu.org/licenses/gpl-3.0.html
 * Domain Path: /lang
 * Text Domain: powershop_filter_import
 */

if ( ! defined( 'WPINC' ) ) {
	die;
}

/*
 * Check if WooCommerce and Powershop plugins are active
 */
if (
	in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) &&
	in_array( 'powershop/powershop.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {

	add_action( 'admin_init', function(){
		// Atributo de Woocommerce usado para los tipos de talla
		register_setting( 'powershop-settings-group', 'powershop_code_to' );
		register_setting( 'powershop-settings-group', 'powershop_code_from' );
		register_setting( 'powershop-settings-group', 'powershop_businesses' );
	} );

	add_action( 'powershop_settings_after', function(){

		//$attributes = wc_get_attribute_taxonomies();
		?>

		<h2><?= __('Opciones de Filtrado de importación', 'powershop');?></h2>

		<table class="form-table">

			<tr valign="top">
				<th scope="row"><?= __('Código desde', 'powershop');?></th>
				<td><input type="text" name="powershop_code_from" value="<?php echo esc_attr( get_option('powershop_code_from') ); ?>" /></td>
			</tr>
			<tr valign="top">
				<th scope="row"><?= __('Código hasta', 'powershop');?></th>
				<td><input type="text" name="powershop_code_to" value="<?php echo esc_attr( get_option('powershop_code_to') ); ?>" /></td>
			</tr>
			<tr valign="top">
				<th scope="row"><?= __('Negocios', 'powershop');?></th>
				<td>
					<input type="text" name="powershop_businesses" value="<?php echo esc_attr( get_option('powershop_businesses') ); ?>" />
					<p><?= __('Añade los negocios que hay que incluir en la importación de los productos, separados por comas y sin espacios', 'powershop');?></p>
				</td>
			</tr>

		</table>
		<?php
	});

	add_filter( 'powershop_get_ps_products_where_clause', function($where){

		$powershop_code_from = esc_attr(get_option('powershop_code_from'));
		$powershop_code_to = esc_attr(get_option('powershop_code_to'));
		$powershop_businesses = esc_attr(get_option('powershop_businesses'));
		$business_where_clause = '';

		if ( $powershop_businesses ) {
			
			$powershop_businesses = explode( ',', $powershop_businesses );	
			
			if ( is_array( $powershop_businesses ) && count( $powershop_businesses ) ) {

				$business_where_clause = [];
				foreach ( $powershop_businesses as $business ) {
					$business_where_clause[] = "CriterioX = '$business'";
				}	

				$business_where_clause = '(' . implode( ' OR ', $business_where_clause ) . ') AND';
			}
		}

		return "WHERE $business_where_clause (Codigo > '$powershop_code_from' AND Codigo < '$powershop_code_to')";

		$import_filter_where_clause = esc_attr(get_option('powershop_import_where_clause'));
		
	}, 10 );
}


/*
 * Check for updates
 */ 
if ( is_admin() ) {
	define( 'PEANUT_UPDATES_URL', 'http://updates.peanutbutter.es' );
	require plugin_dir_path( __FILE__ ) . 'includes/plugin-update-checker/plugin-update-checker.php';
	$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
		PEANUT_UPDATES_URL.'/?action=get_metadata&slug=powershop-filter-import',
		__FILE__, //Full path to the main plugin file or functions.php.
		'powershop-filter-import'
	);
}
